FROM python:3.9.12

LABEL maintainer="Bruno Cabado <sr.brunocabado@gmail.com>"

COPY . /starbars/
WORKDIR /starbars/

RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["main.py"]