# StarBars backend

<img src="logo.png" alt="drawing" width="200"/>

## Installation

### Requirements

- Python >= 3.7
- Pip
- Git

### Clone repository

By ssh:

```bash
git@gitlab.com:starbars/backend.git
```

or

By http:

```bash
https://gitlab.com/starbars/backend.git
```

### Set virtual environment and install dependencies

```bash
python -m venv env
source env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

### Set needed credentials

Add a `.env` file with the following information:

```bash
GOOGLE_MAPS_API_KEY={YOUR_GOOGLE_MAPS_API_KEY}
```

## Run the application

### Locally

```bash
python main.py
```

### Docker

- Create the container:

```bash
docker build -t starbars .
```

- Run the container:

```bash
docker run --name starbars --rm --network host -ti starbars:latest
```
