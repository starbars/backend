from typing import Dict


def get_example() -> Dict:
    """Returns an example places request.

    - Returns:
        - **Dict**: Places request.
    """
    return {
        "center_location": {
            "latitude": 43.3429855,
            "longitude": -8.4151679
        },
        "places": [
            {
                "id": "ChIJszT_2r58Lg0RfPdZ92quF6o",
                "location": {
                    "latitude": 43.3429855,
                    "longitude": -8.4151679
                },
                "name": "La Tasca de Mon",
                "rating": 3.4,
                "types": [
                    "bar",
                    "restaurant",
                    "food",
                    "point_of_interest",
                    "establishment"
                ]
            },
            {
                "id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "location": {
                    "latitude": 43.3450934,
                    "longitude": -8.4140214
                },
                "name": "O Bon Humor",
                "rating": 4.1,
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ]
            }
        ]
    }


def get_google_example() -> Dict:
    """Returns an example google places request.

    - Returns:
        - **Dict**: Google places request.
    """
    return {
        "html_attributions": [],
        "results": [
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3429855,
                        "lng": -8.4151679
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3443036302915,
                            "lng": -8.413768019708497
                        },
                        "southwest": {
                            "lat": 43.3416056697085,
                            "lng": -8.416465980291502
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "La Tasca de Mon",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2592,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/105875028344789391207\">Antonio Iglesias Rojo</a>"
                        ],
                        "photo_reference": "Aap_uEAyBB7IR1dTuO1jU5j4nmAe-w6qrTTZAx2DdzA5Z7XtZ5JUjsU1n0pPO5CjLho_luueOKQZ-ImeCXwxjN-v912ZMElgKSETpqVgp_GuL7_Qitk3X__-l_pO760rIrmJ5VVjWXJ7jbM9vyf62FxdcHsMCcqFnWoMtG8_l9Irz9GO8eNF",
                        "width": 1944
                    }
                ],
                "place_id": "ChIJszT_2r58Lg0RfPdZ92quF6o",
                "plus_code": {
                    "compound_code": "8HVM+5W A Coruña, Spain",
                    "global_code": "8CMH8HVM+5W"
                },
                "price_level": 1,
                "rating": 3.4,
                "reference": "ChIJszT_2r58Lg0RfPdZ92quF6o",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "restaurant",
                    "food",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 38,
                "vicinity": "Spain"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            },
            {
                "business_status": "OPERATIONAL",
                "geometry": {
                    "location": {
                        "lat": 43.3450934,
                        "lng": -8.4140214
                    },
                    "viewport": {
                        "northeast": {
                            "lat": 43.3463710802915,
                            "lng": -8.412582869708498
                        },
                        "southwest": {
                            "lat": 43.34367311970851,
                            "lng": -8.4152808302915
                        }
                    }
                },
                "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/bar-71.png",
                "icon_background_color": "#FF9E67",
                "icon_mask_base_uri": "https://maps.gstatic.com/mapfiles/place_api/icons/v2/bar_pinlet",
                "name": "O Bon Humor",
                "opening_hours": {
                        "open_now": True
                },
                "photos": [
                    {
                        "height": 2645,
                        "html_attributions": [
                            "<a href=\"https://maps.google.com/maps/contrib/108047761473433083167\">Ma Mónica Mira Martínez</a>"
                        ],
                        "photo_reference": "Aap_uEAAWGye3I2z90QhW8jCGyQzGMABpSZ-ZaPntc81PZ-KduL4EQvPJ-KeTf46j4aWig0BBxHd5vzru1L5Oo6jk1rpu8Y7YCr-9Ky86ulNEk6Ua_Y_XVE5gz2n522HQdCHR4e-20hM0PPUZtnU6UwNlI55BY8rQwftNy-LdfFW3bomC9pU",
                        "width": 2213
                    }
                ],
                "place_id": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "plus_code": {
                    "compound_code": "8HWP+29 A Coruña, Spain",
                    "global_code": "8CMH8HWP+29"
                },
                "price_level": 1,
                "rating": 4.1,
                "reference": "ChIJcVW8Zr58Lg0R2FnpPjiMLdM",
                "scope": "GOOGLE",
                "types": [
                    "bar",
                    "point_of_interest",
                    "establishment"
                ],
                "user_ratings_total": 37,
                "vicinity": "A, Praza Escultor Mon Vasco, 3, A Coruña"
            }
        ],
        "status": "OK"
    }
