from typing import Optional, List, Dict

import uvicorn
from fastapi import FastAPI

from geopy.geocoders import Nominatim
from dotenv import load_dotenv

from data import examples

from operations import addresses
from operations import locations
from operations import places

app = FastAPI(docs_url="/")
geolocator = Nominatim(user_agent="StarBars", timeout=10)

load_dotenv()


@app.get("/location/test")
def get_test_location() -> Dict:
    """Returns a location for testing purposes.

    - Returns:
        - **Dict**: Geological location.
    """
    return locations.get_test_location()


@app.get("/location")
def get_location_from_address(address_name: str) -> Dict:
    """Returns a locations based on a received address name.

    - Args:
        - **address_name (str)**: Search address name.

    - Returns:
        - **Dict**: Geological location.
    """
    return locations.get_location_from_address(geolocator, address_name)


@app.post("/location/center")
def get_center_location_from_addresses(addresse_names: List[str]) -> Dict:
    """Returns the centered location for the giving address names.

    - Args:
        - **addresse_names (List[str])**: List of search address names.

    - Returns:
        - **Dict**: Centered geological location.
    """
    return locations.get_center_location_from_addresses(geolocator, addresse_names)


@app.get("/address")
def get_address(address_name: str) -> Dict:
    """Returns the full address name from a short address name.

    - Args:
        - **address_name (str)**: Search address name.

    - Returns:
        - **Dict**: Full address name.
    """
    return addresses.get_full_address(geolocator, address_name)


@app.post("/addresses")
def get_addresses(address_names: List[str]) -> Dict:
    """Returns the full address names from a short address names.

    - Args:
        - **address_names (List[str])**: List of search address names.

    - Returns:
        - **Dict**: Full address names.
    """
    return addresses.get_full_addresses(geolocator, address_names)


@app.get("/places/test")
def get_test_places() -> Dict:
    """Return places for testing purposes.

    - Returns:
        - **Dict**: Places information.
    """
    return examples.get_example()


@app.post("/places/search")
def get_places(address_names: List[str], results_number: Optional[int] = 5, places_type: Optional[str] = "bar", radius_meters: Optional[int] = 500) -> Dict:
    """Get the centered location and the defined near places information for a giving list of address names.

    - Args:
        - **address_names (List[str])**: List of search address names.
        - **results_number (Optional[int])**: Number of maximum results.  Defaults to 5.
        - **places_type (Optional[str])**: Places type.  Defaults to "bar".
        - **radius_meters (Optional[int], optional)**: Maximum radius in meter for search from centered point. Defaults to 500.

    - Returns:
        - **Dict**: Centered point and near places information.
    """
    return places.get_places_from_addresses(geolocator, address_names, results_number, places_type, radius_meters)


def main() -> None:
    """Executes the main application loop
    """
    uvicorn.run("main:app", host="127.0.0.1", port=8888, reload=True)


if __name__ == "__main__":
    main()
