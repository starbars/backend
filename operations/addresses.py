from geopy.geocoders.base import Geocoder
from typing import Dict, List


def get_full_address(geolocator: Geocoder, address_name: str) -> Dict[str, str]:
    """Returns the full address name from a short address name.

    - Args:
        - **geolocator (Geocoder)**: Geological location.
        - **address_name (str)**: Search address name.

    - Returns:
        **Dict[str, str]**: Full address name.
    """
    location = geolocator.geocode(address_name)
    return {"address": location.address}


def get_full_addresses(geolocator: Geocoder, address_names: List[str]) -> Dict[str, List[str]]:
    """Returns the full address names from a short address names.

    - Args:
        - geolocator (Geocoder): Geological location.
        - **address_names (List[str])**: List of search address names.

    - Returns:
        - **Dict[str, List[str]]**: Full address names.
    """
    addresses = []
    for address in address_names:
        addresses.append(geolocator.geocode(address).address)
    return {"addresses": addresses}
