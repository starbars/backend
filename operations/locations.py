from geopy.geocoders.base import Geocoder
from typing import Dict, List
from loguru import logger


def get_test_location() -> Dict[str, Dict[str, float]]:
    """Returns a location for testing purposes.

    - Returns:
        - **Dict[str, Dict[str, float]]**: Geological location.
    """
    return {"location": {"latitude": 41.4034789, "longitude": 2.174410333009705}}


def get_location_from_address(geolocator: Geocoder, address_name: str) -> Dict[str, Dict[str, float]]:
    """Returns a locations based on a received address name.

    - Args:
        - **geolocator (Geocoder)**: Geolocator object.
        - **address_name (str)**: _description_

    - Returns:
        - **Dict[str, Dict[str, float]]**: Geological location.
    """
    location = geolocator.geocode(address_name)
    return {"location": {"latitude": location.latitude, "longitude": location.longitude}}


def get_center_location_from_addresses(geolocator: Geocoder, address_names: List[str]) -> Dict[str, Dict[str, float]]:
    """Returns the centered location for the giving address names.

    - Args:
        - **geolocator (Geocoder)**: Geolocator object.
        - **address_names (List[str])**: List of search address names.

    - Returns:
        - **Dict[str, Dict[str, float]]**: Geological location.
    """
    locations = []
    total_latitude = 0.0
    total_longitude = 0.0
    for address in address_names:
        location = geolocator.geocode(address)
        logger.info(location.address)
        locations.append([location.latitude, location.longitude])
        total_latitude += location.latitude
        total_longitude += location.longitude
    center_latitude = total_latitude / len(locations)
    center_longitude = total_longitude / len(locations)
    return {"location": {"latitude": center_latitude, "longitude": center_longitude}}
