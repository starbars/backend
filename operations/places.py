from geopy.geocoders.base import Geocoder
from typing import Optional, Dict, List

import os
import requests
import json

from data import examples
from operations import locations
from custom_types import places_types

from loguru import logger


def get_places_from_addresses(geolocator: Geocoder, address_names: List[str], results_number: Optional[int] = 5, places_type: Optional[str] = "bar", radius_meters: Optional[int] = 500) -> Dict:
    """Get the centered location and the near places information for a giving list of address names.

    - Args:
        - **geolocator** (Geocoder): Geolocator object.
        - **address_names (List[str])**: List of search address names.
        - **results_number (Optional[int])**: Number of maximum results. Defaults to 5.
        - **places_type (Optional[str])**: Places type.  Defaults to "bar".
        - **radius_meters (Optional[int], optional)**: Maximum radius in meter for search from centered point. Defaults to 500.

    - Returns:
        - **Dict**: Centered point and near places information.
    """
    GOOGLE_MAPS_API_KEY = os.getenv('GOOGLE_MAPS_API_KEY')
    center_location = locations.get_center_location_from_addresses(
        geolocator, address_names)
    center_latitude = center_location["location"]["latitude"]
    center_longitude = center_location["location"]["longitude"]

    final_json = {}
    final_json.update(
        {"center_location": {"latitude": center_latitude, "longitude": center_longitude}})

    places_type = places_type if places_type in places_types.get_place_types() else "bar"
    google_url = f"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={center_latitude}%2C{center_longitude}&radius={radius_meters}&type={places_type}&opennow&key={GOOGLE_MAPS_API_KEY}"
    response = requests.request("GET", google_url, headers={}, data={})
    response_json = response.json()
    filtered_results = []
    for result in response_json["results"]:
        filtered_result = {}
        filtered_result.update({"id": result["place_id"]})
        filtered_result.update({"location": {
            "latitude": result["geometry"]["location"]["lat"],
            "longitude": result["geometry"]["location"]["lng"]
        }})
        filtered_result.update({"name": result["name"]})
        if "rating" in result:
            filtered_result.update({"rating": result["rating"]})
        else:
            filtered_result.update({"rating": 0.0})
        filtered_result.update({"types": result["types"]})
        filtered_results.append(filtered_result)
    ordered_results = sorted(
        filtered_results, key=lambda x: x["rating"], reverse=True)
    final_json.update({"places": ordered_results[0:results_number]})
    logger.debug(json.dumps(final_json, indent=4))
    return final_json
